# 2019FRC_README_DOCS

## wpilib page: 
Most useful page for programmers.  
https://wpilib.screenstepslive.com

### How to's for Java  programming.
https:// wpilib.screenstepslive.com/s/currentCS/m/java


## Notes on Windows installs:

### Step 0: 
 -  make sure you have WIFI connected
 -  Windows OS is Updated to latest patch
 -  You will download a lot of data maybe around 8GB in total.

### Step 1:
install NI tools [install time 2hr]
 - Read and follow https://wpilib.screenstepslive.com/s/currentCS/m/java/l/1027504-installing-the-frc-update-suite-all-languages
 - In summary :
    -  [Skip if it is new laptop] Uninstall Old Versions of NI tools
    -  Install LABVIEW [install time 1hr]
        - Log into ni.com and 'Create account' .
        - get the serial number :
        - use links for ni suite install : 
            - https://wpilib.screenstepslive.com/s/currentCS/m/labview/l/1027501-installing-labview-for-frc-2019-labview-only
            - download file 5GB: http://www.ni.com/download/labview-for-frc-18.0/7841/en/ 
            - teams wishing to use NI Vision Assistant should run the full Suite installer
        - NI Update Service patches to LabVIEW. **It is NOT recommended **
            to install these updates unless directed by FRC through 
            our usual communication channels (Frank's Blog, Team Updates or E-mail Blasts).
    - install FRC Update Suite contains  [install time 1hr]
        -   https://wpilib.screenstepslive.com/s/currentCS/m/java/l/1027504-installing-the-frc-update-suite-all-languages
        -   Download the update from http://www.ni.com/download/first-robotics-software-2017/7904/en/
        -   The key is:   $Robots&in#SPACE!!
        -   select all Product List
    - Note:  serial number : its team based serial number: ask Mr Gray or Mentor Mano:
    - Note: The Driver Station will only work on Windows



### Step 2:
Ms visual code and wpilib install
- below link covers most of the tools. Pick the file Based on the OS type [32/64 bit] of your laptop
    - https://github.com/wpilibsuite/allwpilib/releases

### Step 3:TODO


## Programming/coding for our Robot :

### Understand command mode coding :
- read: https://wpilib.screenstepslive.com/s/currentCS/m/java/l/599732-what-is-command-based-programming
- http://first.wpi.edu/FRC/roborio/release/docs/java/
- 


### VS code shortcuts for FRC: try them all:
https://wpilib.screenstepslive.com/s/currentCS/m/java/l/1027552-wpilib-commands-in-vscode

### Understand how to plug a joystick and input keys in to the code:
- finalize the joystick we will use and input keys we will have
- https://wpilib.screenstepslive.com/s/currentCS/m/java/c/88894

### Understand Drive types suitable for our ROBOT:
- get all the drive types: once with library given by WPILIB and once we have to write from scratch.
    - may be tank drive is best for this year: comment what you think. 
    - 


### Understand RoboRIO and Sensors
- https://www.thecompassalliance.org/single-post/2018/11/18/Fall-Workshops-2018---RoboRIO-and-Sensors
